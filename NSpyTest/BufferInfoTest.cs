﻿using NSpyEdit;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace NSpyTest
{
    
    
    /// <summary>
    ///This is a test class for BufferInfoTest and is intended
    ///to contain all BufferInfoTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BufferInfoTest
    {
        private const string FileName = //@"d:\Projects\prj-csharp\NSpyEdit\NSpyTest\TestData\text.spy";
                                        @"c:\Projects\prj-csharp\NSpyEdit\NSpyTest\TestData\text.spy";

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        ///A test for loadFromFile
        ///</summary>
        [TestMethod()]
        public void loadFromFileTest()
        {
            string passw = "123"; // TODO: Initialize to an appropriate value
            BufferInfo actual;
            actual = BufferInfo.loadFromFile(FileName, passw);
            Console.WriteLine(actual.DecodedData);
            Console.WriteLine("End of test");
        }

        /// <summary>
        ///A test for saveToFile
        ///</summary>
        [TestMethod()]
        public void saveToFileTest()
        {
            string passw = "123"; // TODO: Initialize to an appropriate value
            BufferInfo target = new BufferInfo(passw, "привет"); // TODO: Initialize to an appropriate value
            target.saveAs(FileName);
        }

        /// <summary>
        ///A test for DecodedData
        ///</summary>
        [TestMethod()]
        public void TestEncodeDecode()
        {
            string passw = "123";
            string text1 = "Here is some data to encrypt!--LL привет",
                   text2 = "Here is some data",
                   text3 = "Here is",
                   text4 = "привет Here is some data to encrypt!",
                   text5 = "привет Here is some data",
                   text6 = "привет Here is",
                   text7 = "привет";
            saveAndReadTest(passw, text1);
            saveAndReadTest(passw, text2);
            saveAndReadTest(passw, text3);
            saveAndReadTest(passw, text4);
            saveAndReadTest(passw, text5);
            saveAndReadTest(passw, text6);
            saveAndReadTest(passw, text7);
        }

        private void saveAndReadTest(string passw, string text)
        {
            Console.WriteLine("Test encrypt: " + text);
            string file_name = @"d:\Projects\prj-csharp\NSpyEdit\NSpyTest\TestData\text2.spy";
            BufferInfo expected = new BufferInfo(passw, text); // TODO: Initialize to an appropriate value
            expected.saveAs(file_name);
            BufferInfo actual = BufferInfo.loadFromFile(file_name, passw);
            Assert.AreEqual(expected.DecodedData, actual.DecodedData);
        }
    }
}
