﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NSpyTest
{
    [TestClass]
    public class Samples
    {
        [TestMethod]
        public void TestDates()
        {
            DateTime start_dt = new DateTime(2007, 1, 1);
            
            int[,] table = new int[7, 31];

            start_dt = start_dt.Date;
            DateTime end_dt = start_dt.AddYears(11);
            for (; start_dt < end_dt; start_dt = start_dt.AddDays(1.0).Date)
                table[(int)start_dt.DayOfWeek, start_dt.Day - 1]++;
            Console.WriteLine("Число Вс. Пн. Ср. Чт. Пт. Сб.");
            for (int i = 0; i < 31; i++)
            {
                Console.WriteLine("{0} {1} {2} {3} {4} {5} {6} {7}", i + 1,
                    table[0, i], table[1, i], table[2, i], table[3, i], table[4, i], table[5, i], table[6, i]);
            }
        }
    }
}
