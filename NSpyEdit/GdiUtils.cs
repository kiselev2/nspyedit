﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;

namespace NSpyEdit
{
    public class GdiUtils
    {
        public static double getLineHeight(Font f)
        {
            return (f.Size * f.FontFamily.GetLineSpacing(FontStyle.Regular)) / f.FontFamily.GetEmHeight(FontStyle.Regular);
        }

        public static double getLineWidth(Control c, string s)
        {
            using (Graphics g = c.CreateGraphics())
            {
                SizeF size = g.MeasureString(s, c.Font);
                return size.Width;
            }
        }
    }
}
