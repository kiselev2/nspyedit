﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NSpyEdit.TxtManagers
{
    public static class DocConstants
    {
        public const int LastPageIndex = -1;
    }

    public interface IPagedDocument
    {
        /// <summary>
        /// Получить количество страниц в документе
        /// </summary>
        /// <returns>кол-во страниц</returns>
        int getPagesCount();

        /// <summary>
        /// получить страницу по индексу
        /// </summary>
        /// <param name="index">индекс страницы</param>
        /// <returns>объект страницы</returns>
        ITextPage getPage(int index);
        /// <summary>
        /// удалить страницу по номеру
        /// </summary>
        void removePage(int page_index = DocConstants.LastPageIndex);
        /// <summary>
        /// вставить страницу
        /// </summary>
        /// <param name="page"></param>
        void insertPage(ITextPage page, int page_index = DocConstants.LastPageIndex);
    }
}
