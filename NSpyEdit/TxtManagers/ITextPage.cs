﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NSpyEdit.TxtManagers
{
    public interface ITextPage
    {
        Action<ITextPage> OnTextChange { get; set; }
        int PageNumber { get; set; }
        string setText(string text);

        int PageWidthInChars { get; set; }
        int PageHeightInLines { get; set; }
    }
}
