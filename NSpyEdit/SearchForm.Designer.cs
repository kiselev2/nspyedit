﻿namespace NSpyEdit
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchForm));
            this.SearchCtrl = new NSpyEdit.UC_Controls.SearchControl();
            this.SuspendLayout();
            // 
            // SearchCtrl
            // 
            this.SearchCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchCtrl.IsCaseSensitive = false;
            this.SearchCtrl.IsForwardSearchDirection = true;
            this.SearchCtrl.IsSearchFullWords = false;
            this.SearchCtrl.Location = new System.Drawing.Point(10, 1);
            this.SearchCtrl.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.SearchCtrl.Name = "SearchCtrl";
            this.SearchCtrl.OnFindClick = null;
            this.SearchCtrl.OnReplaceClick = null;
            this.SearchCtrl.PupupListSize = 15;
            this.SearchCtrl.Size = new System.Drawing.Size(337, 140);
            this.SearchCtrl.TabIndex = 0;
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(357, 142);
            this.Controls.Add(this.SearchCtrl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SearchForm";
            this.Padding = new System.Windows.Forms.Padding(10, 1, 10, 1);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Search/Replace";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SearchForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        public UC_Controls.SearchControl SearchCtrl;


    }
}