﻿namespace NSpyEdit.UC_Controls
{
    partial class SearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_find = new System.Windows.Forms.ComboBox();
            this.lb_hdr_search = new System.Windows.Forms.Label();
            this.cb_replace = new System.Windows.Forms.ComboBox();
            this.chb_case_sensitive = new System.Windows.Forms.CheckBox();
            this.chb_full_word = new System.Windows.Forms.CheckBox();
            this.bt_next = new System.Windows.Forms.Button();
            this.bt_replace = new System.Windows.Forms.Button();
            this.lb_replace = new System.Windows.Forms.Label();
            this.grb_direction = new System.Windows.Forms.GroupBox();
            this.rbt_backward = new System.Windows.Forms.RadioButton();
            this.rbt_forward = new System.Windows.Forms.RadioButton();
            this.grb_direction.SuspendLayout();
            this.SuspendLayout();
            // 
            // cb_find
            // 
            this.cb_find.FormattingEnabled = true;
            this.cb_find.Location = new System.Drawing.Point(3, 27);
            this.cb_find.Name = "cb_find";
            this.cb_find.Size = new System.Drawing.Size(378, 28);
            this.cb_find.TabIndex = 0;
            // 
            // lb_hdr_search
            // 
            this.lb_hdr_search.AutoSize = true;
            this.lb_hdr_search.Location = new System.Drawing.Point(3, 4);
            this.lb_hdr_search.Name = "lb_hdr_search";
            this.lb_hdr_search.Size = new System.Drawing.Size(44, 20);
            this.lb_hdr_search.TabIndex = 7;
            this.lb_hdr_search.Text = "Find:";
            // 
            // cb_replace
            // 
            this.cb_replace.FormattingEnabled = true;
            this.cb_replace.Location = new System.Drawing.Point(3, 168);
            this.cb_replace.Name = "cb_replace";
            this.cb_replace.Size = new System.Drawing.Size(378, 28);
            this.cb_replace.TabIndex = 8;
            // 
            // chb_case_sensitive
            // 
            this.chb_case_sensitive.AutoSize = true;
            this.chb_case_sensitive.Location = new System.Drawing.Point(7, 61);
            this.chb_case_sensitive.Name = "chb_case_sensitive";
            this.chb_case_sensitive.Size = new System.Drawing.Size(140, 24);
            this.chb_case_sensitive.TabIndex = 10;
            this.chb_case_sensitive.Text = "Case Sensitive";
            this.chb_case_sensitive.UseVisualStyleBackColor = true;
            // 
            // chb_full_word
            // 
            this.chb_full_word.AutoSize = true;
            this.chb_full_word.Location = new System.Drawing.Point(181, 61);
            this.chb_full_word.Name = "chb_full_word";
            this.chb_full_word.Size = new System.Drawing.Size(200, 24);
            this.chb_full_word.TabIndex = 11;
            this.chb_full_word.Text = "Search Full Words Only";
            this.chb_full_word.UseVisualStyleBackColor = true;
            // 
            // bt_next
            // 
            this.bt_next.AutoSize = true;
            this.bt_next.Location = new System.Drawing.Point(387, 27);
            this.bt_next.Name = "bt_next";
            this.bt_next.Size = new System.Drawing.Size(107, 30);
            this.bt_next.TabIndex = 1;
            this.bt_next.Text = "Next";
            this.bt_next.UseVisualStyleBackColor = true;
            this.bt_next.Click += new System.EventHandler(this.bt_next_Click);
            // 
            // bt_replace
            // 
            this.bt_replace.AutoSize = true;
            this.bt_replace.Location = new System.Drawing.Point(387, 168);
            this.bt_replace.Name = "bt_replace";
            this.bt_replace.Size = new System.Drawing.Size(107, 30);
            this.bt_replace.TabIndex = 13;
            this.bt_replace.Text = "Replace";
            this.bt_replace.UseVisualStyleBackColor = true;
            this.bt_replace.Click += new System.EventHandler(this.bt_replace_Click);
            // 
            // lb_replace
            // 
            this.lb_replace.AutoSize = true;
            this.lb_replace.Location = new System.Drawing.Point(3, 145);
            this.lb_replace.Name = "lb_replace";
            this.lb_replace.Size = new System.Drawing.Size(72, 20);
            this.lb_replace.TabIndex = 15;
            this.lb_replace.Text = "Replace:";
            // 
            // grb_direction
            // 
            this.grb_direction.Controls.Add(this.rbt_backward);
            this.grb_direction.Controls.Add(this.rbt_forward);
            this.grb_direction.Location = new System.Drawing.Point(3, 91);
            this.grb_direction.Name = "grb_direction";
            this.grb_direction.Size = new System.Drawing.Size(374, 51);
            this.grb_direction.TabIndex = 16;
            this.grb_direction.TabStop = false;
            this.grb_direction.Text = "Search direction";
            // 
            // rbt_backward
            // 
            this.rbt_backward.AutoSize = true;
            this.rbt_backward.Location = new System.Drawing.Point(164, 22);
            this.rbt_backward.Name = "rbt_backward";
            this.rbt_backward.Size = new System.Drawing.Size(104, 24);
            this.rbt_backward.TabIndex = 1;
            this.rbt_backward.TabStop = true;
            this.rbt_backward.Text = "Backward";
            this.rbt_backward.UseVisualStyleBackColor = true;
            // 
            // rbt_forward
            // 
            this.rbt_forward.AutoSize = true;
            this.rbt_forward.Checked = true;
            this.rbt_forward.Location = new System.Drawing.Point(18, 21);
            this.rbt_forward.Name = "rbt_forward";
            this.rbt_forward.Size = new System.Drawing.Size(92, 24);
            this.rbt_forward.TabIndex = 0;
            this.rbt_forward.TabStop = true;
            this.rbt_forward.Text = "Forward";
            this.rbt_forward.UseVisualStyleBackColor = true;
            // 
            // SearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grb_direction);
            this.Controls.Add(this.lb_replace);
            this.Controls.Add(this.bt_replace);
            this.Controls.Add(this.chb_full_word);
            this.Controls.Add(this.chb_case_sensitive);
            this.Controls.Add(this.cb_replace);
            this.Controls.Add(this.lb_hdr_search);
            this.Controls.Add(this.bt_next);
            this.Controls.Add(this.cb_find);
            this.Name = "SearchControl";
            this.Size = new System.Drawing.Size(500, 201);
            this.grb_direction.ResumeLayout(false);
            this.grb_direction.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_find;
        private System.Windows.Forms.Button bt_next;
        private System.Windows.Forms.Label lb_hdr_search;
        private System.Windows.Forms.ComboBox cb_replace;
        private System.Windows.Forms.CheckBox chb_case_sensitive;
        private System.Windows.Forms.CheckBox chb_full_word;
        private System.Windows.Forms.Button bt_replace;
        private System.Windows.Forms.Label lb_replace;
        private System.Windows.Forms.GroupBox grb_direction;
        private System.Windows.Forms.RadioButton rbt_backward;
        private System.Windows.Forms.RadioButton rbt_forward;
    }
}
