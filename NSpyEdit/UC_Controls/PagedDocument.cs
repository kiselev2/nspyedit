﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NSpyEdit.TxtManagers;

namespace NSpyEdit.UC_Controls
{
    public partial class PagedDocument : UserControl, IPagedDocument
    {
        private readonly List<ITextPage> m_pages = new List<ITextPage>();
        public PagedDocument()
        {
            InitializeComponent();
        }

        public int getPagesCount() { return tlp_pages.Controls.Count; }

        public ITextPage getPage(int index)
        {
            return m_pages[index];
        }

        public void removePage(int page_index = DocConstants.LastPageIndex) 
        {
            if (page_index == DocConstants.LastPageIndex)
                page_index = getPagesCount() - 1;
            if (page_index >= 0)
            {
                tlp_pages.Controls.RemoveAt(page_index);
                Control c = m_pages[page_index] as Control;
                if(c != null)
                    c.Dispose();
                this.tlp_pages.SuspendLayout();
                m_pages.RemoveAt(page_index);
                for (int i = page_index; i < m_pages.Count; i++)
                {
                    tlp_pages.Controls.Remove(m_pages[i] as Control);
                    tlp_pages.Controls.Add(m_pages[i] as Control, 0, i - 1);
                }
                this.tlp_pages.ResumeLayout();
                this.tlp_pages.PerformLayout();
                tlp_pages.RowCount--;
            }
        }

        public void insertPage(ITextPage page, int page_index = DocConstants.LastPageIndex)
        {
            if (page_index == DocConstants.LastPageIndex)
                page_index = getPagesCount();
            tlp_pages.RowCount++;
            if (page_index != getPagesCount())
            {
                for (int i = getPagesCount(); i >= page_index; --i)
                {
                    tlp_pages.Controls.Remove(m_pages[i] as Control);
                    tlp_pages.Controls.Add(m_pages[i] as Control, 0, i - 1);
                }
            }
            this.tlp_pages.SuspendLayout();
            tlp_pages.Controls.Add(page as Control, 0, page_index);
            this.tlp_pages.RowStyles.Add(new RowStyle());
            this.tlp_pages.ResumeLayout();
            this.tlp_pages.PerformLayout();
            m_pages.Insert(page_index, page);
        }
    }
}
