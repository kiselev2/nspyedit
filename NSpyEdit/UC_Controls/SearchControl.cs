﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace NSpyEdit.UC_Controls
{
    public partial class SearchControl : UserControl
    {
        public Action<SearchControl> OnFindClick { get; set; }
        public Action<SearchControl> OnReplaceClick { get; set; }

        public SearchControl()
        {
            InitializeComponent();
            PupupListSize = 15;
        }

        [Browsable(false)]
        public int PupupListSize { get; set; }
        
        [Browsable(false)]
        public bool IsCaseSensitive 
        {
            get { return chb_case_sensitive.Checked; }
            set { chb_case_sensitive.Checked = value; }
        }

        [Browsable(false)]
        public bool IsSearchFullWords 
        {
            get { return chb_full_word.Checked; }
            set { chb_full_word.Checked = value; } 
        }

        [Browsable(false)]
        public bool IsForwardSearchDirection
        {
            get { return rbt_forward.Checked; }
            set 
            {
                if (value)
                    rbt_forward.Checked = true;
                else
                    rbt_backward.Checked = true;
            }
        }

        public string getToFind()
        {
            return cb_find.Text;
        }

        public string getToReplace()
        {
            return cb_replace.Text;
        }

        public void addToFind(string to_find_value)
        {
            addToCb(cb_find, to_find_value);
        }

        public void addToReplace(string to_replace_value)
        {
            addToCb(cb_replace, to_replace_value);
        }

        private void addToCb(ComboBox cb, string value)
        {
            if (string.IsNullOrEmpty(value))
                return;
            if (cb.Items.Contains(value))
                return;
            cb.Items.Insert(0, value);
            if (cb.Items.Count > PupupListSize)
                cb.Items.RemoveAt(cb.Items.Count - 1);
        }

        private void bt_next_Click(object sender, EventArgs e)
        {
            addToFind(this.getToFind());
            SomeUtils.actionCatchWrapper(() => this.OnFindClick(this), "Search");
        }

        private void bt_replace_Click(object sender, EventArgs e)
        {
            addToReplace(this.getToReplace());
            SomeUtils.actionCatchWrapper(() => this.OnReplaceClick(this), "Search");
        }
    }
}
