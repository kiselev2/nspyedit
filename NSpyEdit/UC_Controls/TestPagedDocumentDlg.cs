﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NSpyEdit.UC_Controls;

namespace NSpyEdit.UC_Controls
{
    public partial class TestPagedDocumentDlg : Form
    {
        public TestPagedDocumentDlg()
        {
            InitializeComponent();
        }

        private void bt_insert_page_Click(object sender, EventArgs e)
        {
            TextPage page = new TextPage((int)nm_up_down.Value + 1);
            pgd_text_doc.insertPage(page, (int)nm_up_down.Value);
            nm_up_down.Maximum = pgd_text_doc.getPagesCount();
        }

        private void bt_remove_page_Click(object sender, EventArgs e)
        {
            pgd_text_doc.removePage((int)nm_up_down.Value - 1);
            nm_up_down.Maximum = pgd_text_doc.getPagesCount();
        }
    }
}
