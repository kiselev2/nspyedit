﻿namespace NSpyEdit.UC_Controls
{
    partial class TestPagedDocumentDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_insert_page = new System.Windows.Forms.Button();
            this.bt_remove_page = new System.Windows.Forms.Button();
            this.nm_up_down = new System.Windows.Forms.NumericUpDown();
            this.lb_pagenum = new System.Windows.Forms.Label();
            this.pgd_text_doc = new NSpyEdit.UC_Controls.PagedDocument();
            ((System.ComponentModel.ISupportInitialize)(this.nm_up_down)).BeginInit();
            this.SuspendLayout();
            // 
            // bt_insert_page
            // 
            this.bt_insert_page.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_insert_page.Location = new System.Drawing.Point(430, 278);
            this.bt_insert_page.Name = "bt_insert_page";
            this.bt_insert_page.Size = new System.Drawing.Size(75, 23);
            this.bt_insert_page.TabIndex = 1;
            this.bt_insert_page.Text = "Insert page";
            this.bt_insert_page.UseVisualStyleBackColor = true;
            this.bt_insert_page.Click += new System.EventHandler(this.bt_insert_page_Click);
            // 
            // bt_remove_page
            // 
            this.bt_remove_page.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bt_remove_page.Location = new System.Drawing.Point(511, 278);
            this.bt_remove_page.Name = "bt_remove_page";
            this.bt_remove_page.Size = new System.Drawing.Size(90, 23);
            this.bt_remove_page.TabIndex = 2;
            this.bt_remove_page.Text = "Remove page";
            this.bt_remove_page.UseVisualStyleBackColor = true;
            this.bt_remove_page.Click += new System.EventHandler(this.bt_remove_page_Click);
            // 
            // nm_up_down
            // 
            this.nm_up_down.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nm_up_down.Location = new System.Drawing.Point(333, 281);
            this.nm_up_down.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nm_up_down.Name = "nm_up_down";
            this.nm_up_down.Size = new System.Drawing.Size(72, 20);
            this.nm_up_down.TabIndex = 3;
            // 
            // lb_pagenum
            // 
            this.lb_pagenum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_pagenum.AutoSize = true;
            this.lb_pagenum.Location = new System.Drawing.Point(257, 283);
            this.lb_pagenum.Name = "lb_pagenum";
            this.lb_pagenum.Size = new System.Drawing.Size(70, 13);
            this.lb_pagenum.TabIndex = 4;
            this.lb_pagenum.Text = "Page number";
            // 
            // pgd_text_doc
            // 
            this.pgd_text_doc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pgd_text_doc.Location = new System.Drawing.Point(1, 1);
            this.pgd_text_doc.Name = "pgd_text_doc";
            this.pgd_text_doc.Size = new System.Drawing.Size(610, 262);
            this.pgd_text_doc.TabIndex = 0;
            // 
            // TestPagedDocumentDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 313);
            this.Controls.Add(this.lb_pagenum);
            this.Controls.Add(this.nm_up_down);
            this.Controls.Add(this.bt_remove_page);
            this.Controls.Add(this.bt_insert_page);
            this.Controls.Add(this.pgd_text_doc);
            this.Name = "TestPagedDocumentDlg";
            this.Text = "TestPagedDocumentDlg";
            ((System.ComponentModel.ISupportInitialize)(this.nm_up_down)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PagedDocument pgd_text_doc;
        private System.Windows.Forms.Button bt_insert_page;
        private System.Windows.Forms.Button bt_remove_page;
        private System.Windows.Forms.NumericUpDown nm_up_down;
        private System.Windows.Forms.Label lb_pagenum;
    }
}