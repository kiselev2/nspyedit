﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ITextPage = NSpyEdit.TxtManagers.ITextPage;
using NSpyEdit;

namespace NSpyEdit.UC_Controls
{
    public partial class TextPage : UserControl, ITextPage
    {
        private static readonly char[] SAMPLE_CHARS = new char[] { 'Q', 'O', 'M', 'W', 'G', 'U', 'Ц', 'Щ', 'Ш', 'М', 'Ы', 'Ф', 'Ж' };
        private string[] SAMPLE_STRINGS;

        private const int DEFAULT_PAGE_WIDTH = 85,
                          DEFAULT_PAGE_HEIGHT = 52;
        private int m_page_width;

        public Action<ITextPage> OnTextChange { get; set; }

        public TextPage(int number)
        {
            InitializeComponent();
            SAMPLE_STRINGS = new string[SAMPLE_CHARS.Length];
            PageNumber = number;
            lb_pagenum.Text = PageNumber.ToString();
            PageWidthInChars = DEFAULT_PAGE_WIDTH;
            PageHeightInLines = DEFAULT_PAGE_HEIGHT;

            
             
        }
        [Browsable(false)]
        public int PageNumber 
        {
            get
            {
                Func<object> a = () =>
                    {
                        try { return int.Parse(lb_pagenum.Text); }
                        catch { lb_pagenum.Text = "1"; return 1; }
                    };
                if (lb_pagenum.InvokeRequired)
                    return (int)lb_pagenum.Invoke(a);
                else
                    return (int)a();
            }
            set 
            {
                Action<object> a = (o) => lb_pagenum.Text = o.ToString();
                if (lb_pagenum.InvokeRequired)
                    lb_pagenum.Invoke(a, value);
                else
                    a(value);
            }
        }

        [Browsable(false)]
        public int PageWidthInChars 
        {
            get { return m_page_width; }
            set 
            {
                m_page_width = value;
                for (int i = 0; i < SAMPLE_CHARS.Length; i++)
                    SAMPLE_STRINGS[i] = new string(SAMPLE_CHARS[i], m_page_width);
            }
        }

        [Browsable(false)]
        public int PageHeightInLines 
        { 
            get;
            set;
        }

        /// <summary>
        /// Задает текст в контрол в том объеме, в котором он там помещается.
        /// "лишний" текст удаляем из контрола и возвращаем. Если весь текст помещается, возвращаем пустую строку
        /// </summary>
        /// <param name="text">Текст, помещаемый в контрол</param>
        /// <returns>Текст, не поместившийся в контрол</returns>
        public string setText(string text)
        {
            Func<object> a = () =>
                {
                    int char_cnt = PageWidthInChars * PageHeightInLines;
                    try
                    {
                        if (text.Length <= char_cnt)
                            return string.Empty;
                        while (char_cnt > 0)
                        {
                            if (char.IsWhiteSpace(text[char_cnt]))
                                break;
                            char_cnt--;
                        }
                        if (char_cnt == 0)
                            char_cnt = PageWidthInChars * PageHeightInLines;
                        return new string(text.Skip(char_cnt).ToArray());
                    }
                    finally
                    {
                        rtb_page.Text = new string(text.Take(char_cnt).ToArray());
                    }
                };
            string res = string.Empty;
            if (this.InvokeRequired)
            { res = this.Invoke(a) as string; }
            else
            { res = a() as string; }
            return res;
        }

        private void rtb_page_TextChanged(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<TextPage>(Program.ProgName, OnTextChange, this);
        }

        private void rtb_page_FontChanged(object sender, EventArgs e)
        {
            int dw = rtb_page.Width - rtb_page.ClientRectangle.Width,
                dh = rtb_page.Height - rtb_page.ClientRectangle.Height;

            double max_w = 0;
            foreach (string s in SAMPLE_STRINGS)
            {
                double w = GdiUtils.getLineWidth(rtb_page, s);
                if (w > max_w)
                    max_w = w;
            }
            double line_h = GdiUtils.getLineHeight(rtb_page.Font);
            int client_h = (int)Math.Ceiling(line_h * PageHeightInLines),
                client_w = (int)Math.Ceiling(max_w);
            rtb_page.Size = new Size(client_w + dw, client_h + dh);
        }
    }
}
