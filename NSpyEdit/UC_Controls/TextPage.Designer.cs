﻿namespace NSpyEdit.UC_Controls
{
    partial class TextPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_page = new System.Windows.Forms.RichTextBox();
            this.tblp_page = new System.Windows.Forms.TableLayoutPanel();
            this.lb_pagenum = new System.Windows.Forms.Label();
            this.tblp_page.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtb_page
            // 
            this.rtb_page.Location = new System.Drawing.Point(79, 36);
            this.rtb_page.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtb_page.Name = "rtb_page";
            this.rtb_page.Size = new System.Drawing.Size(601, 712);
            this.rtb_page.TabIndex = 0;
            this.rtb_page.Text = "";
            this.rtb_page.FontChanged += new System.EventHandler(this.rtb_page_FontChanged);
            this.rtb_page.TextChanged += new System.EventHandler(this.rtb_page_TextChanged);
            // 
            // tblp_page
            // 
            this.tblp_page.AutoSize = true;
            this.tblp_page.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tblp_page.ColumnCount = 3;
            this.tblp_page.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblp_page.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblp_page.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblp_page.Controls.Add(this.rtb_page, 1, 1);
            this.tblp_page.Controls.Add(this.lb_pagenum, 2, 1);
            this.tblp_page.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblp_page.Location = new System.Drawing.Point(0, 0);
            this.tblp_page.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tblp_page.Name = "tblp_page";
            this.tblp_page.RowCount = 3;
            this.tblp_page.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tblp_page.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblp_page.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tblp_page.Size = new System.Drawing.Size(759, 784);
            this.tblp_page.TabIndex = 2;
            // 
            // lb_pagenum
            // 
            this.lb_pagenum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_pagenum.AutoSize = true;
            this.lb_pagenum.Location = new System.Drawing.Point(688, 722);
            this.lb_pagenum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_pagenum.Name = "lb_pagenum";
            this.lb_pagenum.Padding = new System.Windows.Forms.Padding(0, 0, 0, 11);
            this.lb_pagenum.Size = new System.Drawing.Size(67, 31);
            this.lb_pagenum.TabIndex = 1;
            this.lb_pagenum.Text = "pg_num";
            // 
            // TextPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tblp_page);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "TextPage";
            this.Size = new System.Drawing.Size(759, 784);
            this.tblp_page.ResumeLayout(false);
            this.tblp_page.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_page;
        private System.Windows.Forms.TableLayoutPanel tblp_page;
        private System.Windows.Forms.Label lb_pagenum;
    }
}
