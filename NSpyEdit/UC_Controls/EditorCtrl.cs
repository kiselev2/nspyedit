﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NSpyEdit.UC_Controls
{
    public partial class EditorCtrl : UserControl
    {

        
        public EditorCtrl()
        {
            InitializeComponent();
        }

        public Action<EditorCtrl> OnTextChange {  get; set; }

        private void rtb_text_TextChanged(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<EditorCtrl>(Program.ProgName, OnTextChange, this);
        }

        public string FullText
        { get { return rtb_text.Text; } }

        public void selectText(int start, int length)
        {
            rtb_text.Select(start, length);
            rtb_text.ScrollToCaret();
        }

        public KeyValuePair<int, int> getSelection()
        {
            return new KeyValuePair<int, int>(rtb_text.SelectionStart, rtb_text.SelectionLength);
        }

        public Point[] getSelectionRect()
        {
            Point p1 = rtb_text.GetPositionFromCharIndex(rtb_text.SelectionStart),
                  p2 = rtb_text.GetPositionFromCharIndex(rtb_text.SelectionStart + rtb_text.SelectionLength);
            int line_height = (int)((rtb_text.Font.Size * rtb_text.Font.FontFamily.GetLineSpacing(FontStyle.Regular)) / rtb_text.Font.FontFamily.GetEmHeight(FontStyle.Regular));
            return new Point[]{ p1, p2, new Point(p1.X, p1.Y + line_height), new Point(p2.X, p2.Y + line_height)};
        }

        public string[] TextLines
        {
            get { return rtb_text.Lines; }
            set 
            { 
                rtb_text.Lines = value;
                updateWordsStat();
            }
        }

        public void replaceSelection(string txt)
        {
            rtb_text.SelectedText = txt;
        }

        public Color EditorBackColor
        {
            get { return rtb_text.BackColor; }
            set { rtb_text.BackColor = value; }
        }

        public Color EditorFontColor
        {
            get { return rtb_text.ForeColor; }
            set { rtb_text.ForeColor = value; }
        }

        public Font EditorFont
        {
            get { return rtb_text.Font; }
            set { rtb_text.Font = value; }
        }

        private void rtb_text_SelectionChanged(object sender, EventArgs e)
        {
            int pos = rtb_text.SelectionStart + rtb_text.SelectionLength;
            
            string[] txt_before = rtb_text.Text.Substring(0, pos).Split('\n');
            int line = txt_before.Length == 0 ? 1 : txt_before.Length;
            int column = txt_before.Length == 0 ? 1 : txt_before[line - 1].Length + 1;
            stat_lb_cur_line.Text = string.Format("Line: {0}", line);
            stat_lb_cur_column.Text = string.Format("Column: {0}", column);
            updateWordsStat();
        }

        private void updateWordsStat()
        {
            int words = rtb_text.Text.Split(new char[] { ' ', '\n', '!', ',', ':', '.', ';', '?', '"', '\'' }, StringSplitOptions.RemoveEmptyEntries).Length;
            stat_lb_words.Text = string.Format("Total words: {0}", words);
            stat_lb_symbols.Text = string.Format("Total symbols: {0}", rtb_text.Text.Length);
        }
    }
}
