﻿namespace NSpyEdit.UC_Controls
{
    partial class EditorCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_text = new System.Windows.Forms.RichTextBox();
            this.stat_strip = new System.Windows.Forms.StatusStrip();
            this.stat_lb_cur_line = new System.Windows.Forms.ToolStripStatusLabel();
            this.stat_lb_cur_column = new System.Windows.Forms.ToolStripStatusLabel();
            this.stat_lb_words = new System.Windows.Forms.ToolStripStatusLabel();
            this.stat_lb_symbols = new System.Windows.Forms.ToolStripStatusLabel();
            this.stat_strip.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtb_text
            // 
            this.rtb_text.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtb_text.AutoWordSelection = true;
            this.rtb_text.BackColor = System.Drawing.Color.Olive;
            this.rtb_text.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtb_text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.rtb_text.HideSelection = false;
            this.rtb_text.Location = new System.Drawing.Point(0, 0);
            this.rtb_text.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtb_text.Name = "rtb_text";
            this.rtb_text.Size = new System.Drawing.Size(848, 459);
            this.rtb_text.TabIndex = 0;
            this.rtb_text.Text = "";
            this.rtb_text.SelectionChanged += new System.EventHandler(this.rtb_text_SelectionChanged);
            this.rtb_text.TextChanged += new System.EventHandler(this.rtb_text_TextChanged);
            // 
            // stat_strip
            // 
            this.stat_strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stat_lb_cur_line,
            this.stat_lb_cur_column,
            this.stat_lb_words,
            this.stat_lb_symbols});
            this.stat_strip.Location = new System.Drawing.Point(0, 473);
            this.stat_strip.Name = "stat_strip";
            this.stat_strip.Padding = new System.Windows.Forms.Padding(2, 0, 21, 0);
            this.stat_strip.Size = new System.Drawing.Size(850, 22);
            this.stat_strip.TabIndex = 1;
            this.stat_strip.Text = "statusStrip1";
            // 
            // stat_lb_cur_line
            // 
            this.stat_lb_cur_line.AutoSize = false;
            this.stat_lb_cur_line.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stat_lb_cur_line.Name = "stat_lb_cur_line";
            this.stat_lb_cur_line.Size = new System.Drawing.Size(109, 17);
            this.stat_lb_cur_line.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stat_lb_cur_column
            // 
            this.stat_lb_cur_column.AutoSize = false;
            this.stat_lb_cur_column.Name = "stat_lb_cur_column";
            this.stat_lb_cur_column.Size = new System.Drawing.Size(109, 17);
            // 
            // stat_lb_words
            // 
            this.stat_lb_words.AutoSize = false;
            this.stat_lb_words.Name = "stat_lb_words";
            this.stat_lb_words.Size = new System.Drawing.Size(109, 17);
            // 
            // stat_lb_symbols
            // 
            this.stat_lb_symbols.Name = "stat_lb_symbols";
            this.stat_lb_symbols.Size = new System.Drawing.Size(0, 17);
            // 
            // EditorCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.stat_strip);
            this.Controls.Add(this.rtb_text);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "EditorCtrl";
            this.Size = new System.Drawing.Size(850, 495);
            this.stat_strip.ResumeLayout(false);
            this.stat_strip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_text;
        private System.Windows.Forms.StatusStrip stat_strip;
        private System.Windows.Forms.ToolStripStatusLabel stat_lb_cur_line;
        private System.Windows.Forms.ToolStripStatusLabel stat_lb_cur_column;
        private System.Windows.Forms.ToolStripStatusLabel stat_lb_words;
        private System.Windows.Forms.ToolStripStatusLabel stat_lb_symbols;
    }
}
