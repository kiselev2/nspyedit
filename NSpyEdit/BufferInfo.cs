﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace NSpyEdit
{
    public class KeySource
    {
        public const int BLOCK_LENGTH = 16;
        public byte[] Key { get; private set; }
        public byte[] IV { get; private set; }

        public KeySource(string passw)
        {
            if (string.IsNullOrEmpty(passw))
                throw new InvalidOperationException("Password can't be empty.");
            string tmp_passw = passw.Length >= BLOCK_LENGTH ? passw : passw + new string('a', BLOCK_LENGTH - passw.Length);
            byte[] passw_data = (new UTF8Encoding()).GetBytes(tmp_passw);
            using ( var prov = new SHA1CryptoServiceProvider())
            { Key = prov.ComputeHash(passw_data); }
            Key = resizeKey(Key);

            using (var prov = new SHA1CryptoServiceProvider())
            { IV = prov.ComputeHash(Key);        }
            IV = IV.Take(BLOCK_LENGTH).ToArray();// resizeForAes(IV);
        }

        private static byte[] resizeKey(byte[] src)
        { 
            const int KEY_SIZE = 2*BLOCK_LENGTH;
            if (src.Length >= KEY_SIZE)
                return src.Take(KEY_SIZE).ToArray();
            int offset = KEY_SIZE - src.Length;
            byte[] res = new byte[KEY_SIZE];
            Array.Copy(src, res, src.Length);
            for (int i = offset; i < res.Length; i++)
                res[i] ^= src[i - offset];
            return res;
        }

    }

    public class BufferInfo
    {
        public const int SHA1_LEN = 20;
        public const string PADDING_SPLITTER = "\0\0";
        public const int MAX_PADDING = 256;
        public byte[] DataSHA1 { get; private set; }

        public byte[] EncodedData { get; private set; }

        public string DecodedData { get; set; }

        public readonly bool LoadBufferSuccess;

        private readonly KeySource m_key_source;

        public BufferInfo(string passw, byte[] data)
        {
            m_key_source = new KeySource(passw);
            if (data.Length <= SHA1_LEN)
                throw new InvalidOperationException("Data can not be empty");
            DataSHA1 = data.Take(SHA1_LEN).ToArray();
            EncodedData = data.Skip(SHA1_LEN).ToArray();
            string padded_text;
            using (AesCryptoServiceProvider aes_alg = new AesCryptoServiceProvider())
            {
                aes_alg.Key = m_key_source.Key;
                aes_alg.IV = m_key_source.IV;
                //aes_alg.BlockSize = 128;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aes_alg.CreateDecryptor(aes_alg.Key, aes_alg.IV);

                // Create the streams used for decryption. 
                
                using (MemoryStream ms_decrypt = new MemoryStream(EncodedData))
                {
                    using (CryptoStream cs_decrypt = new CryptoStream(ms_decrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader sr_decrypt = new StreamReader(cs_decrypt))
                        {
                            padded_text = sr_decrypt.ReadToEnd();
                        }
                    }
                }
                
            }
            DecodedData = cutPadding(padded_text);
            byte[] padded_bytes = (new UTF8Encoding()).GetBytes(padded_text);
            using (var prov = new SHA1CryptoServiceProvider())
            {
                byte [] decoded_sha = prov.ComputeHash(padded_bytes);
                int sum = 0;
                for (int i = 0; i < SHA1_LEN; i++)
                    sum += DataSHA1[i] ^ decoded_sha[i];
                LoadBufferSuccess = sum == 0;
            }
        }

        public BufferInfo(string passw, string text)
        {
            m_key_source = new KeySource(passw);
            DecodedData = text;
            FileName = null;
            LoadBufferSuccess = true;
        }

        public string FileName { get; private set; }

        public void updateEncoded()
        {
            string to_encode = appendPadding(DecodedData);

            byte[] to_encode_bytes = (new UTF8Encoding()).GetBytes(to_encode);
            using (var prov = new SHA1CryptoServiceProvider())
            {
                DataSHA1 = prov.ComputeHash(to_encode_bytes);
            }

            using (AesCryptoServiceProvider aes_alg = new AesCryptoServiceProvider())
            {
                aes_alg.Key = m_key_source.Key;
                aes_alg.IV = m_key_source.IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aes_alg.CreateEncryptor(aes_alg.Key, aes_alg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream ms_encrypt = new MemoryStream())
                {
                    using (CryptoStream cs_encrypt = new CryptoStream(ms_encrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter sw_encrypt = new StreamWriter(cs_encrypt))
                        {
                            sw_encrypt.Write(to_encode);
                        }
                        EncodedData = ms_encrypt.ToArray();
                    }
                }
            }

        }

        private static string appendPadding(string data)
        {
            data += PADDING_SPLITTER;
            int char_cnt = new System.Random((int)DateTime.Now.ToFileTime()).Next(MAX_PADDING);
            byte[] rand_data = new byte[char_cnt];
            new System.Random((int)DateTime.Now.ToFileTime()).NextBytes(rand_data);
            data += new string(rand_data.ToList().ConvertAll<char>(c => (char)c).ToArray());
            return data;
        }

        private static string cutPadding(string data)
        {
            string tail =
                new string(data.SkipWhile((c, i) => i < data.Length - 1 && !(data[i + 1] == '\0' && data[i] == '\0')).ToArray());
            return data.Substring(0, data.Length - tail.Length);
        }

        public static BufferInfo loadFromFile(string file_name, string passw)
        {
            int read_step = 8192;
            byte [] buf = new byte[read_step];
            List<byte> res = new List<byte>();
            using (FileStream fs = new FileStream(file_name, FileMode.Open, FileAccess.Read))
            {
                while (true)
                {
                    int actual_read = fs.Read(buf, 0, read_step);
                    if (actual_read == read_step)
                        res.AddRange(buf);
                    else
                    {
                        if (actual_read > 0)
                            res.AddRange(buf.Take(actual_read));
                        break;
                    }
                }
            }
            var res_buf = new BufferInfo(passw, res.ToArray());
            res_buf.FileName = file_name;
            return res_buf;
        }

        public void save()
        { 
            updateEncoded();
            using (FileStream fs = new FileStream(FileName, FileMode.Create))
            {
                fs.Write(DataSHA1, 0, DataSHA1.Length);
                fs.Write(EncodedData, 0, EncodedData.Length);
            }
        }

        public void saveAs(string file_name)
        {
            FileName = file_name;
            save();
        }

        public string[] getTextLines()
        {
            return DecodedData.Split(new char[] { '\n' }, StringSplitOptions.None);
        }
    }
}
