﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using NSpyEdit.Actions;

namespace NSpyEdit
{
    public partial class MainForm : Form, ITextSelectable
    {
        private bool m_exit_with_reset = false;
        public Action<MainForm> OnNewFile { get; set; }
        public Action<MainForm> OnOpenFile { get; set; }
        public Action<MainForm> OnSaveFile { get; set; }
        public Action<MainForm> OnSaveAsFile { get; set; }
        public Action<MainForm> OnExitAndSaveFile { get; set; }
        public Action<MainForm> OnTextChange {get;set;}

        public Action<MainForm> OnSelBackgroundColor { get; set; }
        public Action<MainForm> OnSelFontColor { get; set; }
        public Action<MainForm> OnSelFont { get; set; }

        public Action<MainForm> OnFindReplace { get; set; }

        public MainForm()
        {
            InitializeComponent();
            ed_ctrl.OnTextChange = (f) =>
                {
                    SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnTextChange, this);
                };
            
        }

        public string[] TextLines 
        {
            get { return ed_ctrl.TextLines; }
            set { ed_ctrl.TextLines = value; }
        }

        public Color EditorBackColor 
        {
            get { return ed_ctrl.EditorBackColor; }
            set { ed_ctrl.EditorBackColor = value; }
        }

        public bool IsTextChanged { get; set; }

        public Color EditorFontColor
        {
            get { return ed_ctrl.EditorFontColor; }
            set { ed_ctrl.EditorFontColor = value; }
        }

        public Font EditorFont
        {
            get { return ed_ctrl.EditorFont; }
            set { ed_ctrl.EditorFont = value; }
        }

        public void replaceSelection(string txt)
        {
            ed_ctrl.replaceSelection(txt);
        }

        public string getText()
        {
            return ed_ctrl.FullText;
        }

        public void selectText(int start, int length)
        {
            ed_ctrl.selectText(start, length);
            ed_ctrl.Focus();
        }

        public KeyValuePair<int, int> getSelection()
        {
            return ed_ctrl.getSelection();
        }

        public Point[] getSelectionRect()
        { 
            return ed_ctrl.getSelectionRect();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnNewFile, this); 
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnOpenFile, this);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnSaveFile, this);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnSaveAsFile, this);
        }

        private void exitAndSaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_exit_with_reset = true;
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnExitAndSaveFile, this);
            Application.Exit();
        }

        private void exitAndResetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_exit_with_reset = true;
            Application.Exit();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!m_exit_with_reset)
                SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnExitAndSaveFile, this);
            e.Cancel = false;
        }

        private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnSelBackgroundColor, this);
        }

        private void fontColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnSelFontColor, this);
        }

        private void fontSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnSelFont, this);
        }

        private void findReplaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SomeUtils.catchWrapper<MainForm>(Program.ProgName, OnFindReplace, this);
        }
    }
}
