﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NSpyEdit
{
    static class Program
    {
        public const string ProgName = "NSpyEdit";
        public const string REG_PATH = "Software/NSpyEdit";
        [STAThread]
        static void Main(string [] pars)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainFormFactory factory = new MainFormFactory();
            Form form = factory.getInstance();
            if (pars.Length > 0 && !string.IsNullOrEmpty(pars[0]))
                factory.openFile((MainForm)form, pars[0]);
            Application.Run(form);
        }
    }
}
