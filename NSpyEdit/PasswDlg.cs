﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NSpyEdit
{
    public partial class PasswDlg : Form
    {
        private bool m_show_confirm;
        public bool Success { get; private set; }

        public PasswDlg(bool show_confirm)
        {
            InitializeComponent();
            Success = false;
            m_show_confirm = show_confirm;
            lb_confirm.Visible = 
            txb_confirm.Visible = m_show_confirm;
        }

        public string Password { get { return txb_password.Text; } }

        public string PwdConfirm { get { return txb_confirm.Text; } }

        private void bt_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bt_ok_Click(object sender, EventArgs e)
        {
            Action<PasswDlg> a = (pd) =>
                {
                    onEnterPassw(pd);
                    this.Close();
                };
            SomeUtils.catchWrapper<PasswDlg>(Program.ProgName, a, this);
        }

        private void onEnterPassw(PasswDlg pd)
        {
            if (pd.Password != pd.PwdConfirm && m_show_confirm)
            {
                Success = false;
                throw new InvalidOperationException("Password and Confirm Password fields are not equal");
            }
            Success = true;
        }
    }
}
