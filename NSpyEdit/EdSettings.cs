﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace NSpyEdit
{
    public class EdSettings
    {
        public const string LAST_OPENED_DIR = "LastOpenedDir",
                            RGB_FNT_COLOR = "RGBFontColor",
                            RGB_BKGND_COLOR = "RGBBckndColor",
                            FONT_SIZE = "FontSize",
                            FONT_NAME = "FontName",
                            FONT_EFFECTS = "FontEffects",
                            IS_SEARCH_CASE_SENSITIVE = "IsSearchCaseSensitive",
                            IS_SEARCH_FULL_WORDS = "IsSearchFullWords",
                            IS_SEARCH_FORWARD = "IsSearchForward";
        public const string DEF_DIR = "c:\\";
        public const string DEF_FNT_NAME = "Comic Sans MS";
        public const float DEF_FNT_SIZE = 11.25f;
        public const FontStyle DEF_FNT_EFF = FontStyle.Bold;
        public static readonly Color DEF_FONT_COLOR,
                                   DEF_BKGND_COLOR;

        public string LastOpenedDir { get; set; }
        public Color RGBFontColor { get; set; }
        public Color RGBBckndColor { get; set; }

        public string FontName{get;set;}
        public float FontSize { get; set; }
        public FontStyle FontEffects{get;set;}

        public bool IsCaseSensitive { get; set; }
        public bool IsSearchFullWords { get; set; }
        public bool IsSearchForward { get; set; }

        static EdSettings()
        { 
            DEF_FONT_COLOR = Color.FromArgb(0,0,192);
            DEF_BKGND_COLOR = Color.Olive;
        }

        public static EdSettings loadSettings()
        {
            EdSettings res = new EdSettings()
            {
                LastOpenedDir = DEF_DIR,
                RGBBckndColor = DEF_BKGND_COLOR,
                RGBFontColor = DEF_FONT_COLOR,
                FontName = DEF_FNT_NAME,
                FontSize = DEF_FNT_SIZE,
                FontEffects = DEF_FNT_EFF
            };
            try { res.LastOpenedDir = (string)SomeUtils.getRegParam(LAST_OPENED_DIR, DEF_DIR); }
            catch { }
            try { res.RGBBckndColor = Color.FromArgb((int)SomeUtils.getRegParam(RGB_BKGND_COLOR, DEF_BKGND_COLOR.ToArgb())); }
            catch { }
            try { res.RGBFontColor = Color.FromArgb((int)SomeUtils.getRegParam(RGB_FNT_COLOR, DEF_FONT_COLOR.ToArgb())); }
            catch { }
            try { res.FontSize = (float)((int)SomeUtils.getRegParam(FONT_SIZE, (int)(DEF_FNT_SIZE * 100))) / 100.0f; }
            catch { }
            try { res.FontName = (string)SomeUtils.getRegParam(FONT_NAME, DEF_FNT_NAME); }
            catch { }
            try { res.FontEffects = (FontStyle)(int)SomeUtils.getRegParam(FONT_EFFECTS, (int)DEF_FNT_EFF); }
            catch { }
            try { res.IsCaseSensitive = (int)SomeUtils.getRegParam(IS_SEARCH_CASE_SENSITIVE, 0) > 0; }
            catch { }
            try { res.IsSearchFullWords = (int)SomeUtils.getRegParam(IS_SEARCH_FULL_WORDS, 0) > 0; }
            catch { }
            try { res.IsSearchForward = (int)SomeUtils.getRegParam(IS_SEARCH_FORWARD, 1) > 0; }
            catch { }
            return res;
        }

        public void saveSettings()
        {
            SomeUtils.setRegParam(LAST_OPENED_DIR, this.LastOpenedDir);
            SomeUtils.setRegParam(RGB_BKGND_COLOR, this.RGBBckndColor.ToArgb());
            SomeUtils.setRegParam(RGB_FNT_COLOR, this.RGBFontColor.ToArgb());
            SomeUtils.setRegParam(FONT_SIZE, (int)(this.FontSize*100+0.0001));
            SomeUtils.setRegParam(FONT_NAME, this.FontName);
            SomeUtils.setRegParam(FONT_EFFECTS, (int)this.FontEffects);
            SomeUtils.setRegParam(IS_SEARCH_CASE_SENSITIVE, this.IsCaseSensitive ? 1 : 0);
            SomeUtils.setRegParam(IS_SEARCH_FULL_WORDS, this.IsSearchFullWords ? 1 : 0);
        }
    }
}
