﻿namespace NSpyEdit
{
    partial class PasswDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasswDlg));
            this.txb_password = new System.Windows.Forms.MaskedTextBox();
            this.lb_passw = new System.Windows.Forms.Label();
            this.lb_confirm = new System.Windows.Forms.Label();
            this.txb_confirm = new System.Windows.Forms.MaskedTextBox();
            this.bt_ok = new System.Windows.Forms.Button();
            this.bt_cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txb_password
            // 
            this.txb_password.Location = new System.Drawing.Point(127, 9);
            this.txb_password.Name = "txb_password";
            this.txb_password.PasswordChar = '*';
            this.txb_password.Size = new System.Drawing.Size(212, 21);
            this.txb_password.TabIndex = 0;
            // 
            // lb_passw
            // 
            this.lb_passw.AutoSize = true;
            this.lb_passw.Location = new System.Drawing.Point(21, 12);
            this.lb_passw.Name = "lb_passw";
            this.lb_passw.Size = new System.Drawing.Size(100, 15);
            this.lb_passw.TabIndex = 1;
            this.lb_passw.Text = "Enter password:";
            // 
            // lb_confirm
            // 
            this.lb_confirm.AutoSize = true;
            this.lb_confirm.Location = new System.Drawing.Point(6, 43);
            this.lb_confirm.Name = "lb_confirm";
            this.lb_confirm.Size = new System.Drawing.Size(115, 15);
            this.lb_confirm.TabIndex = 2;
            this.lb_confirm.Text = "Confirm password:";
            // 
            // txb_confirm
            // 
            this.txb_confirm.Location = new System.Drawing.Point(127, 40);
            this.txb_confirm.Name = "txb_confirm";
            this.txb_confirm.PasswordChar = '*';
            this.txb_confirm.Size = new System.Drawing.Size(212, 21);
            this.txb_confirm.TabIndex = 3;
            // 
            // bt_ok
            // 
            this.bt_ok.Location = new System.Drawing.Point(372, 8);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(75, 23);
            this.bt_ok.TabIndex = 4;
            this.bt_ok.Text = "OK";
            this.bt_ok.UseVisualStyleBackColor = true;
            this.bt_ok.Click += new System.EventHandler(this.bt_ok_Click);
            // 
            // bt_cancel
            // 
            this.bt_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_cancel.Location = new System.Drawing.Point(372, 40);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(75, 23);
            this.bt_cancel.TabIndex = 5;
            this.bt_cancel.Text = "Cancel";
            this.bt_cancel.UseVisualStyleBackColor = true;
            this.bt_cancel.Click += new System.EventHandler(this.bt_cancel_Click);
            // 
            // PasswDlg
            // 
            this.AcceptButton = this.bt_ok;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bt_cancel;
            this.ClientSize = new System.Drawing.Size(459, 82);
            this.ControlBox = false;
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.txb_confirm);
            this.Controls.Add(this.lb_confirm);
            this.Controls.Add(this.lb_passw);
            this.Controls.Add(this.txb_password);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PasswDlg";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enter password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txb_password;
        private System.Windows.Forms.Label lb_passw;
        private System.Windows.Forms.Label lb_confirm;
        private System.Windows.Forms.MaskedTextBox txb_confirm;
        private System.Windows.Forms.Button bt_ok;
        private System.Windows.Forms.Button bt_cancel;
    }
}