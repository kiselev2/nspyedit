﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Win32;

namespace NSpyEdit
{
    public class SomeUtils
    {
        public static object getRegParam(string name, object defaultValue)
        {
            RegistryKey rk;
            try
            {
                rk = Registry.CurrentUser.OpenSubKey(Program.REG_PATH);
                object s = rk.GetValue(name, defaultValue);
                rk.Close();
                if (s != null) return s;
            }
            catch
            { }
            return defaultValue;
        }
        //---------------------------------------------------------------------------------------
        public static void setRegParam(string name, object value)
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser.CreateSubKey(Program.REG_PATH, RegistryKeyPermissionCheck.ReadWriteSubTree);
                if ((value is long) || (value is int)) rk.SetValue(name, value, RegistryValueKind.DWord);
                else rk.SetValue(name, value, RegistryValueKind.String);
                rk.Close();
            }
            catch
            {
            }
        }
        //---------------------------------------------------------------------------------------
        public static void actionCatchWrapper(Action a, string msgbox_header)
        {
            try
            {
                a();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, msgbox_header, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void catchWrapper<T>(string header, Action<T> action, T param)
        {
            try
            {
                if (action != null)
                    action(param);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occur. Error message: \n" + ex.Message, header, MessageBoxButtons.OK, MessageBoxIcon.Error);
                using (EventLog e_log = new EventLog(Program.ProgName))
                {
                    e_log.Source = Program.ProgName;
                    e_log.WriteEntry("Error occur. \nMessage: \n" + ex.Message + "\n\nStack trace:\n" + ex.StackTrace);
                }
            }
        }
    }
}
