﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace NSpyEdit
{
    public class MainFormFactory
    {
        //public const string LAST_OPENED_DIR = "LastOpenedDir";
        private MainForm m_instance = null;
        private BufferInfo m_buffer = null;
        private bool m_is_buffer_unsaved = false;
        private bool m_file_loading = false;
        private EdSettings m_ed_settings = EdSettings.loadSettings();
        private SearchForm m_search_form;
        public Form getInstance()
        {
            if (m_instance != null)
                return m_instance;
            m_instance = createInstance();
            return m_instance;
        }

        private MainForm createInstance()
        {
            MainForm res = new MainForm()
            {
                EditorBackColor = m_ed_settings.RGBBckndColor,
                EditorFontColor = m_ed_settings.RGBFontColor,
                EditorFont = new Font(m_ed_settings.FontName, m_ed_settings.FontSize, m_ed_settings.FontEffects)
            };
            res.OnOpenFile = onOpenFile;
            res.OnTextChange = onTextChange;
            res.OnNewFile = onNewFile;
            res.OnSaveFile = onSaveFile;
            res.OnExitAndSaveFile = onExitAndSaveFile;
            res.OnSaveAsFile = onSaveAsFile;
            res.OnSelBackgroundColor = onSelBackgroundColor;
            res.OnSelFontColor = onSelFontColor;
            res.OnSelFont = onSelFont;
            res.OnFindReplace = onFindReplace;
            NSpyEdit.Actions.SearchFormFactory sform_factory = new NSpyEdit.Actions.SearchFormFactory(res);
            sform_factory.OnFindAction = (o) => onFindReplace(o as MainForm);
            m_search_form = sform_factory.getInstance();
            return res;
        }

        private void onTextChange(MainForm frm)
        {
            if (!m_file_loading)
            {
                m_is_buffer_unsaved = true;
                frm.Text = string.Format("{0} {1} *", Program.ProgName,
                    m_buffer == null || m_buffer.FileName == null ? "<no name>" : m_buffer.FileName);
            }
        }

        private bool isContinueAfterCheckUnsave(MainForm frm)
        { 
            if (m_is_buffer_unsaved)
            {
                DialogResult res = MessageBox.Show("You have not saved text. Do you wish to save it before continue?\n(Yes - Save, No - Discard not saved text, Cancel - return to edit)",
                    Program.ProgName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                switch (res)
                { 
                    default: // DialogResult.Cancel:
                        return false;//not continue
                    case DialogResult.No:
                        return true;
                    case DialogResult.Yes:
                            onSaveFile(frm);//continue
                        return true;
                }
            }
            return true;
        }

        private void onOpenFile(MainForm frm)
        {
            openFile(frm, null);
        }

        public void openFile(MainForm frm, string file_name)       
        {
            if(!isContinueAfterCheckUnsave(frm))
                return;
            if (string.IsNullOrEmpty(file_name))
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    EdSettings settings = EdSettings.loadSettings();
                    if (!string.IsNullOrEmpty(settings.LastOpenedDir))
                        dlg.InitialDirectory = settings.LastOpenedDir;
                    dlg.Filter = "secret text files(*.spy)|*.spy";
                    if (dlg.ShowDialog() != DialogResult.OK)
                        return;
                    file_name = dlg.FileName;
                    settings.LastOpenedDir = new FileInfo(file_name).DirectoryName;
                    settings.saveSettings();
                }
            }
            string passw;
            while (true)
            {
                try
                {
                    using (PasswDlg dlg = new PasswDlg(false))
                    {
                        dlg.ShowDialog();
                        if (!dlg.Success)
                            return;
                        passw = dlg.Password;
                    }

                    BufferInfo bf = BufferInfo.loadFromFile(file_name, passw);
                    if (bf.LoadBufferSuccess)
                    {
                        m_buffer = bf;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is System.Security.Cryptography.CryptographicException)
                    {
                        System.Threading.Thread.Sleep(1500);
                        MessageBox.Show("Invalid password. Try again.", Program.ProgName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        MessageBox.Show("Error: \n" + ex.Message, Program.ProgName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            m_file_loading = true;
            frm.TextLines = m_buffer.getTextLines();
            m_file_loading = false;
            m_is_buffer_unsaved = false;
            frm.Text = string.Format("{0} {1}", Program.ProgName, m_buffer.FileName);
        }

        private void onNewFile(MainForm frm)
        { 
            if(!isContinueAfterCheckUnsave(frm))
                return;
            m_buffer = null;
            frm.TextLines = new string[0];
        }

        private void onSelBackgroundColor(MainForm frm)
        {
            using (ColorDialog dlg = new ColorDialog())
            {
                dlg.Color = m_ed_settings.RGBBckndColor;
                if (dlg.ShowDialog() != DialogResult.OK)
                    return;
                m_ed_settings.RGBBckndColor = dlg.Color;
            }
            frm.EditorBackColor = m_ed_settings.RGBBckndColor;
            m_ed_settings.saveSettings();
        }

        private void onSelFontColor(MainForm frm)
        {
            using (ColorDialog dlg = new ColorDialog() )
            {
                dlg.Color = m_ed_settings.RGBFontColor;
                if (dlg.ShowDialog() != DialogResult.OK)
                    return;
                m_ed_settings.RGBFontColor = dlg.Color;
            }
            frm.EditorFontColor = m_ed_settings.RGBFontColor;
            m_ed_settings.saveSettings();
        }

        private void onSelFont(MainForm frm)
        {
            using (FontDialog dlg = new FontDialog())
            {
                dlg.Font = new Font(m_ed_settings.FontName, m_ed_settings.FontSize, m_ed_settings.FontEffects);
                if (dlg.ShowDialog() != DialogResult.OK)
                    return;
                m_ed_settings.FontName = dlg.Font.FontFamily.Name;
                m_ed_settings.FontEffects = dlg.Font.Style;
                m_ed_settings.FontSize = dlg.Font.Size;
                frm.EditorFont = dlg.Font;
                m_ed_settings.saveSettings();
            }
        }

        private void onSaveFile(MainForm frm)
        {
            save(frm, m_buffer == null);
        }

        private void onSaveAsFile(MainForm frm)
        {
            save(frm, true);
        }

        private void onExitAndSaveFile(MainForm frm)
        {
            if (m_is_buffer_unsaved)
                save(frm, m_buffer == null);
        }

        private void save(MainForm frm, bool re_save)
        {
            string text = string.Join("\n", frm.TextLines);
            if (re_save || m_buffer == null || m_buffer.FileName == null)
            {
                string file_name;
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    dlg.Filter = "secret text files(*.spy)|*.spy";
                    if (dlg.ShowDialog() != DialogResult.OK)
                        return;
                    file_name = dlg.FileName;
                }
                string passw;
                while (true)
                {
                    using (PasswDlg dlg = new PasswDlg(true))
                    {
                        dlg.ShowDialog();
                        if (!dlg.Success)
                            return;
                        passw = dlg.Password;
                        break;
                    }
                }
                m_buffer = new BufferInfo(passw, text);
                m_buffer.saveAs(file_name);
            }
            else
            {
                m_buffer.DecodedData = text;
                m_buffer.save();
            }
            EdSettings settings = EdSettings.loadSettings();
            //SomeUtils.setRegParam(LAST_OPENED_DIR, new FileInfo(m_buffer.FileName).DirectoryName);
            settings.LastOpenedDir = new FileInfo(m_buffer.FileName).DirectoryName;
            settings.saveSettings();
            m_is_buffer_unsaved = false;
            frm.Text = string.Format("{0} {1}", Program.ProgName, m_buffer.FileName);
        }

        private void onFindReplace(MainForm frm)
        {
            int border_width = (frm.Width - frm.ClientRectangle.Width) / 2;
            int x = frm.ClientRectangle.Width - m_search_form.Width-20,
                y = frm.ClientRectangle.Y + frm.ClientRectangle.Height - m_search_form.Height - 2 * border_width;
            Point[] points = frm.getSelectionRect();
            if (points.Any(p => p.X > x && x < p.X + m_search_form.Width &&
                             p.Y > y && y < p.Y + m_search_form.Height))
            {
                y = frm.ClientRectangle.Y;
            }            
            m_search_form.Location = frm.PointToScreen(new Point(x, y));
            if(!m_search_form.Visible)
                m_search_form.Show(frm);
        }
    }
}
