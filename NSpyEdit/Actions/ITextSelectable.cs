﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace NSpyEdit.Actions
{
    public interface ITextSelectable
    {
        string getText();
        void selectText(int start, int length);
        KeyValuePair<int, int> getSelection();
        void replaceSelection(string txt);
        Point[] getSelectionRect();
    }
}
