﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using NSpyEdit.UC_Controls;

namespace NSpyEdit.Actions
{
    public class SearchFormFactory
    {
        private SearchForm m_instance = null;
        private readonly ITextSelectable m_editor;

        private object m_search_lock = new object();
        private string m_search_word = string.Empty;
        private List<int> m_search_res_positions = new List<int>();
        const string SEPARATORS = "()[]{}:;,.'\"!@#$%^&?*-_=+\\|/<>`~ \n\r";

        public Action<ITextSelectable> OnFindAction { get; set; }

        public SearchFormFactory(ITextSelectable editor)
        {
            m_editor = editor;
        }

        public SearchForm getInstance()
        {
            if (m_instance == null)
                m_instance = createInstance();

            return m_instance;
        }

        private SearchForm createInstance()
        {
            EdSettings settings = EdSettings.loadSettings();
            SearchForm res = new SearchForm();
            res.SearchCtrl.IsCaseSensitive = settings.IsCaseSensitive;
            res.SearchCtrl.IsSearchFullWords = settings.IsSearchFullWords;
            res.SearchCtrl.IsForwardSearchDirection = settings.IsSearchForward;
            res.SearchCtrl.OnReplaceClick = onReplaceClick;
            res.SearchCtrl.OnFindClick = onFindClick;
            return res;
        }

        private void onReplaceClick(SearchControl ctrl)
        {
            string to_replace = ctrl.getToReplace();
            if (string.IsNullOrEmpty(to_replace))
                return;
            m_editor.replaceSelection(to_replace);
            onFindClick(ctrl);
        }

        private void onFindClick(SearchControl ctrl)
        {
            if (ctrl.IsForwardSearchDirection)
                onSearchForward(ctrl);
            else
                onSearchBackward(ctrl);
            if (OnFindAction != null)
                OnFindAction(m_editor);
        }

        private void onSearchForward(SearchControl ctrl)
        {
            var selection = m_editor.getSelection();
            selection = search(selection.Key + selection.Value, ctrl.getToFind(), m_editor.getText(), ctrl.IsCaseSensitive, ctrl.IsSearchFullWords);
            if(selection.Key >= 0)
                m_editor.selectText(selection.Key, selection.Value);
        }

        private void onSearchBackward(SearchControl ctrl)
        {
            var selection = m_editor.getSelection();
            string search_w = new string(ctrl.getToFind().Reverse().ToArray()),
                   txt = new string(m_editor.getText().Reverse().ToArray());
            selection = search(txt.Length - selection.Key, search_w, txt, ctrl.IsCaseSensitive, ctrl.IsSearchFullWords);
            if (selection.Key >= 0)
            {
                int start_sel = txt.Length - selection.Key - search_w.Length;
                m_editor.selectText(start_sel, selection.Value);
            }
        }

        private KeyValuePair<int, int> search(int start_ind, string search_word, string txt, bool case_sensitive, bool search_full_word)
        {
            if(string.IsNullOrEmpty(search_word))
                return new KeyValuePair<int,int>(-1, -1);
            while (true)
            {
                if (search_word.Length <= txt.Length)
                {
                    start_ind = txt.IndexOf(search_word, start_ind, case_sensitive ? StringComparison.CurrentCulture : StringComparison.CurrentCultureIgnoreCase);
                    if (start_ind >= 0)
                    {
                        if ((start_ind == 0 || SEPARATORS.Contains(txt[start_ind - 1]))
                            && (start_ind + search_word.Length == txt.Length || SEPARATORS.Contains(txt[start_ind + search_word.Length]))
                            || !search_full_word )
                        {
                            return new KeyValuePair<int, int>(start_ind, search_word.Length);
                        }
                        start_ind += search_word.Length;
                        continue;
                    }
                }

                if (DialogResult.No == MessageBox.Show(string.Format("Search word '{0}' not found. Do you wish start from begin of text?", search_word),
                                Program.ProgName, MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                    return new KeyValuePair<int,int>(-1, -1);
                start_ind = 0;
            }
        }
    }
}
