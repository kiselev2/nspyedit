﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EdTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void rtb_text_SelectionChanged(object sender, EventArgs e)
        {
            if (rtb_text.SelectionStart >= 0)
            {
                Point p = rtb_text.GetPositionFromCharIndex(rtb_text.SelectionStart);
                tss_location_x.Text = string.Format("x = {0};", p.X);
                tss_location_y.Text = string.Format("y = {0};", p.Y);
            }
            else
            {
                tss_location_x.Text = tss_location_y.Text = string.Empty;
            }
        }
    }
}
