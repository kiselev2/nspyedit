﻿namespace EdTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_text = new System.Windows.Forms.RichTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tss_location_x = new System.Windows.Forms.ToolStripStatusLabel();
            this.tss_location_y = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtb_text
            // 
            this.rtb_text.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb_text.Location = new System.Drawing.Point(0, 0);
            this.rtb_text.Name = "rtb_text";
            this.rtb_text.Size = new System.Drawing.Size(739, 364);
            this.rtb_text.TabIndex = 0;
            this.rtb_text.Text = "";
            this.rtb_text.SelectionChanged += new System.EventHandler(this.rtb_text_SelectionChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tss_location_x,
            this.tss_location_y});
            this.statusStrip1.Location = new System.Drawing.Point(0, 342);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(739, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tss_location_x
            // 
            this.tss_location_x.Name = "tss_location_x";
            this.tss_location_x.Size = new System.Drawing.Size(20, 17);
            this.tss_location_x.Text = "x=";
            // 
            // tss_location_y
            // 
            this.tss_location_y.Name = "tss_location_y";
            this.tss_location_y.Size = new System.Drawing.Size(21, 17);
            this.tss_location_y.Text = "y=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 364);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.rtb_text);
            this.Name = "Form1";
            this.Text = "Form1";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_text;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tss_location_x;
        private System.Windows.Forms.ToolStripStatusLabel tss_location_y;
    }
}

